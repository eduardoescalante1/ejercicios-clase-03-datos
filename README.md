# Ejercicios para practicar la lectura de archivos y la creación de imágenes

## Instrucciones generales

- La tarea se compone de dos ejercicios propuestos, los cuales están
relacionados entre sí
- La entrega debe ser un archivo html con la solución de los dos ejercicios
generado a partir de un notebook. Ambos archivos deben estar presentes.
Identificar estos archivos con el nombre ENTREGA
para diferenciarlos de otros archivos temporales usados al practicar
- Cada notebook debe ser mucho más que un montón de líneas de comando, debe
contener explicaciones, e iniciar con un encabezado **identificándose** y
describiendo el problema a resolver
- Fraccione el código en celdas de acuerdo a la lógica de la solución
- Explique su estrategia de solución y la funcionalidad de las distintas
partes del código, utilizando comentarios e intercalando celdas de markdown
- Exploraciones complementarias al ejercicio serán muy bien recibidas

**El objetivo es que si su instructor desea correr el código pueda hacerlo,
para eso va el markdown, pero que esto no sea necesario para evaluar la tarea,
para eso va el html con todas las explicaciones y los ejemplos con resultados**

## Ejercicio No. 1

- Investigue sobre el diagrama de Hertzprung-Russell, una herramienta muy
potente en astronomia, y describa un poco al respecto para darle contexto al
resto de la tarea
- El objetivo es generar un diagrama HR lo más parecido al de [esta referencia](https://socratic.org/questions/what-is-the-hertzsprung-russell-diagram-and-why-is-it-so-important-to-astronomy-#277707). No lucirá idéntico por que no se usarán exactamente los mismos datos,
y las unidades pueden ser ligeramente distinta. La idea sí es dejar su figura
lo más parecida a la de referencia en el estilo: colores, escalas en los ejes,
tamaño de los marcadores, leyendas, textos en el gráfico, etc.
- Los datos para crear la figura están en la carpeta `Data`. Cada tabla contiene
las informaciones sobre un tipo de estrellas según indican los nombres de 
archivo. La información viene en 3 columnas: luminosidad en luminosidades
solares, Temperatura en Kelvin y Radio de la estrella en unidades arbitrarias
- La idea es que cada estrella en el gráfico tenga un color representativo
de su temperatura (que estrellas frías son rojas y estrellas calientes
son azules) y que el tamaño del símbolo sea representativo del tamaño de
cada estrella para diferenciar entre enanas, gigantes y estrellas de 
secuencia principal
- Busque que su código sea semi automático; es indispensable leer
los datos desde el propio programa, no copiarlos a mano, y hallar una forma
de obtener los tamaños y colores sin declararlos uno a uno

## Ejercicio No. 2
- Después de tener un diseño de base para el ejercicio No. 1, en este ejercicio
se pide generar una animación, en la cual se reproduzca el miso gráfico de
antes pero las estrellas vayan apareciendo progresivamente


**NOTA: Variantes a estas propuestas serán bien recibidas**






